#!/usr/bin/env python

import os
import csv

CSVDIR = "./csv"
FREPORT = "output/report.html"

fd = open(FREPORT, "w")
fd.write("<html>\n<head>\n</head>\n<body>\n<table border='1'>\n")
fd.write("<h1>Linux updates report</h1>")
fd.write("<th>Hostname</th><th>Updates</th><th>Kernel</th><th>Target Kernel</th><th>Distribution</th><th>Last reboot</th><th>IP</th>\n")


for csvfile in os.listdir(CSVDIR):
    if csvfile.endswith(".csv"):
        print("Parsing : " + CSVDIR+"/"+csvfile)
        with open(CSVDIR+"/"+csvfile) as fdcsv:
            csvreader = csv.reader(fdcsv, delimiter=';')
            line = "<tr>"
            for row in csvreader:
                for item in row :
                    line = line + "<td>" + item + "</td>"
            line = line + "</tr>\n"
            fd.write(line)
        fdcsv.close()
    else:
        continue


fd.write("</table>\n</body>\n</html>\n")
fd.close()

print("\nRapport disponible dans "+FREPORT)
